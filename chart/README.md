# What's Here

- a chart that builds and deploys a small shell container

## But really, what does it all do?

`templates/imagestreams.yaml`
  
- imports `debian:stable` from upstream to use as a base
- creates a target for the buildconfig

`templates/buildconfig.yaml`
  
- creates a buildconfig based on `debian:stable` with changes from the `Dockerfile` right here

`Dockerfile`

- installs a few things one may want in a shell pod!

`templates/deploymentconfig.yaml`

- deploys the newly built image with `replicas=1` which can then be accessed via `oc rsh dc/debian`
