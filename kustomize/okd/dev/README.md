# What is this

it's a base and dev overlay kustomize deployment of my shell pod.

if run like:

```bash
WebHookSecretKey=${WHSK} username=${GITLAB_USER} password=${GITLAB_TOKEN} kubectl apply -k dev
# or
WebHookSecretKey=${WHSK} username=${GITLAB_USER} password=${GITLAB_TOKEN} kustomize build dev

```

then the stuff in the `dev` directory will lay over the top of the things in the `base` directory and you'll get a unique deployment.

NEAT
