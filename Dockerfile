FROM image-mirror-prod-registry.cloud.duke.edu/library/redis:latest as redis

FROM image-mirror-prod-registry.cloud.duke.edu/library/debian:stable

LABEL maintainer='Nate Childers <nate.childers@duke.edu>'

LABEL vendor='Duke University, Openshift Users Group' \
      architecture='x86_64' \
      summary='debian base image, tweaked' \
      description='Base image for Debian stable with some good bits added' \
      distribution-scope='private' \
      authoritative-source-url='https://gitlab.oit.duke.edu/nbc9/debian-shell'

LABEL version='1.0' \
      release='1'

RUN apt-get update && \
    apt-get install --no-install-recommends -y \
      curl \
      dnsutils \
      httpie \
      iputils-ping \
      iproute2 \
      jq \
      libnss-unknown \
      netcat-traditional \
      rsync \
      socat

COPY --from=redis /usr/local/bin/redis-cli /usr/local/bin/

RUN mkdir /.httpie && chgrp -R 0 /.httpie && chmod -R g+rwX /.httpie

ENTRYPOINT ["/bin/bash", "-c", "trap : TERM INT; sleep infinity & wait"]
