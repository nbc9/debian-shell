# What is this?

TLDR; you probably want to fork this and adapt it to your needs but feel free to use the image i build on this repo if you want.

Otherwise, this is what's here:

- `chart/`:
  - a dumb chart that will install the container from this repository into, well an OKD/Openshift cluster.
- `kustomize/`:
  - a kuztomize configuration that will install the container from this repository into, well, an OKD/Openshift cluster also.
- `os/debian-shell{-dc}.yaml`:
  - ImageStream: debian:stable from docker.io (upstream, official Debian image)
  - ImageStream: debian-shell empty place to put customized images based in debian:stable
  - BuildConfig: build-debian-shell - uses Dockerfile from this repo to create a basic container image with stuff that might be needed on a little shell container
    - Output: stable tag on the ImageStream above -> i.e. debian-shell:stable  
    - Needs:
      - Secrets:
        - gitlab-nbc9: deploy secret from gitlab, created manually in gitlab
        - webhook: counterpart of deploy secret, used for gitlab by signal to this build that things need rebuilding, created manually in openshift
      - Volumes:
        - There's a couple of volumes in here too that change a lot. make sure you have those, or better yet yank them out.

## TODO

- make this more generic, e.g. debian-shell with children like nbc9-debian-shell with my own crap in it. Should be easy enough.
- make this kubernetes agnostic?


## Credits

Robots lovingly delivered by [robohash.org]